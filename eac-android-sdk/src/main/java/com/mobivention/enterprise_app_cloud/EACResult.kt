package com.mobivention.enterprise_app_cloud

/**
 * Used to display the various states of the update metadata
 *
 * UpdateNeeded: An update of any kind (mandatory or not is needed)
 * NoUpdateNeeded: The Version is current
 * Error: An error of any kind occurred during the loading of the update metadata
 */
sealed class EACResult {
    class UpdateNeeded(
        val isMandatory: Boolean,
        val version: String?,
        val build: String?,
        val downloadURL: String?
    ) : EACResult()

    object NoUpdateNeeded : EACResult()
    class Error(val message: String?) : EACResult()
}