package com.mobivention.enterprise_app_cloud

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Only callable from a coroutine, checks update metadata at provided server.
 */
object EAC_Handler {

    suspend fun loadDetailsFromEAC(urlToLoad: String, versionCode: Long): EACResult {
        return withContext(Dispatchers.Default) {
            try {
                val data = urlToLoad.getStringObjectFromURL()
                val jo = data.getJSONObjectFormString()
                if (jo != null && jo.getString("build").toLong() > versionCode) {
                    return@withContext EACResult.UpdateNeeded(
                        jo.getBoolean("mandatory"),
                        jo.getString("version"),
                        jo.getString("build"),
                        jo.getString("download_url")
                    )
                } else {
                    return@withContext EACResult.NoUpdateNeeded
                }
            } catch (e: Exception) {
                return@withContext EACResult.Error(e.toString())
            }
        }
    }
}