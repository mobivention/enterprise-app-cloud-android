package com.mobivention.enterprise_app_cloud

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.ComponentActivity
import androidx.lifecycle.lifecycleScope


/**
 * The type Eac(Enterprise App Clound) actions. Used to communicate between your project and the "Enterprise App Cloud"-Service
 */
object EAC_Actions {

    private const val TAG = "EnterpriseAppCloud"

    /**
     * Function that provides default value for the base URL used by the request.
     * Can be overridden to target a custom host
     */
    private fun defaultHost() = if (BuildConfig.DEBUG) {
        "https://eactest.mobivention.eu"
    } else {
        "https://enterprise-app-cloud.com"
    }

    /**
     * Check for enterprise app cloud updates. Displ
     *
     * @param activity Context of the calling Activity
     * @param teamSlug Slug of the team for which to request updates. You cna find your team slug in the API-Tab of the website.
     * @param baseURL The custom url for the "Enterprise App Cloud"-Service, or a default if not overridden
     */
    fun checkForEnterpriseAppCloudUpdates(
        activity: ComponentActivity,
        teamSlug: String,
        baseURL: String = defaultHost()
    ) {
        val packageName = activity.packageName
        val versionCode = activity.getLongVersionCode()
        val urlString = "$baseURL/api/$teamSlug/android/$packageName/current/"
        Log.d(
            TAG,
            "checkForEnterpriseAppCloudUpdates - input\n\turlString: $urlString\n\tpackageName: $packageName\n\tversionCode: $versionCode"
        )
        activity.lifecycleScope.launchWhenCreated {
            when (val result = EAC_Handler.loadDetailsFromEAC(urlString, versionCode)) {
                is EACResult.UpdateNeeded -> {
                    Log.d(
                        TAG,
                        "checkForEnterpriseAppCloudUpdates - onSuccess\n\tisMandatory: ${result.isMandatory}\n"
                    )
                    EAC_Dialog(
                        activity,
                        result.isMandatory,
                        result.version,
                        result.build,
                        result.downloadURL
                    )
                }
                is EACResult.NoUpdateNeeded -> Log.d(
                    TAG,
                    "checkForEnterpriseAppCloudUpdates - onSuccess\n\tup-to-date"
                )
                is EACResult.Error -> Log.d(
                    TAG,
                    "checkForEnterpriseAppCloudUpdates - onError\n\t${result.message}"
                )
            }
        }
    }

    /**
     * Retrieves the current version code of the app as a Long
     *
     * @return Current version code of the app
     */
    private fun Context.getLongVersionCode(): Long {
        return try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                packageManager.getPackageInfo(packageName, 0).longVersionCode
            } else {
                packageManager.getPackageInfo(packageName, 0).versionCode.toLong()
            }
        } catch (nnfe: PackageManager.NameNotFoundException) {
            return 0L
        }
    }

    /**
     * Install app update by opening the provided link inside the users default browser.
     *
     * @param activity    Activity from which to start the update loop from
     * @param isMandatory If the update is mandatory, the activity gets finished when the user gets redirected to the browser.
     * @param downloadURL Url to open in web browser, for this case it will be the url for an apk-file
     */
    fun installAppUpdate(activity: Activity, isMandatory: Boolean, downloadURL: String?) {
        if (!downloadURL.isNullOrBlank()) {
            var url = downloadURL
            //Add https:// if download url does not have it leading the url
            if (!downloadURL.startsWith("http://") && !downloadURL.startsWith("https://")) {
                url = "https://$downloadURL"
            }
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            activity.startActivity(browserIntent)
            if (isMandatory) {
                activity.finish()
            }
        }
    }
}