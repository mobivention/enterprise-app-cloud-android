package com.mobivention.enterprise_app_cloud

import android.app.Activity
import android.app.AlertDialog
import com.mobivention.enterprise_app_cloud.EAC_Actions.installAppUpdate

/**
 * Instantiates and shows new Eac dialog.
 *
 * @param activity    Context of the Activity
 * @param isMandatory true, if update is mandatory
 * @param version     the versionname of the new version
 * @param build       the buildnumber of the new version
 * @param downloadURL the url to download the new version
 */
class EAC_Dialog(
    activity: Activity,
    isMandatory: Boolean,
    version: String?,
    build: String?,
    downloadURL: String?
) {
    init {
        val dialogBuilder = AlertDialog.Builder(activity)
        dialogBuilder.setTitle(activity.getString(R.string.update_popup_title))
        dialogBuilder.setCancelable(false)
        if (isMandatory) {
            dialogBuilder.setMessage(
                activity.getString(
                    R.string.update_popup_text_mandatory,
                    version,
                    build
                )
            )
            dialogBuilder.setPositiveButton(
                activity.getString(R.string.update_popup_install)
            ) { dialogInterface, i ->
                installAppUpdate(activity, true, downloadURL)
                dialogInterface.dismiss()
            }
        } else {
            dialogBuilder.setMessage(
                activity.getString(
                    R.string.update_popup_text_optional,
                    version,
                    build
                )
            )
            dialogBuilder.setPositiveButton(
                activity.getString(R.string.update_popup_yes)
            ) { dialogInterface, i ->
                installAppUpdate(activity, false, downloadURL)
                dialogInterface.dismiss()
            }
            dialogBuilder.setNegativeButton(
                activity.getString(R.string.update_popup_no)
            ) { dialogInterface, i -> dialogInterface.dismiss() }
        }
        dialogBuilder.show()
    }
}