package com.mobivention.enterprise_app_cloud

import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

/**
 * Gets json object form string.
 *
 * @return the json object form string
 * @throws IOException   the io exception
 * @throws JSONException the json exception
 */
@Throws(IOException::class, JSONException::class)
fun String?.getJSONObjectFormString(): JSONObject? {
    return this?.let { JSONObject(it) }
}

/**
 * Returned HTTP-GET as string object a string-formatted url.
 *
 * @return HTTP-GET result
 * @throws IOException the io exception
 */
@Throws(IOException::class)
fun String?.getStringObjectFromURL(): String {
    val urlConnection: HttpURLConnection?
    val url = URL(this)
    urlConnection = url.openConnection() as HttpURLConnection
    urlConnection.requestMethod = "GET"
    urlConnection.readTimeout = 10000
    urlConnection.connectTimeout = 15000
    urlConnection.doOutput = true
    urlConnection.connect()
    var responseString = ""
    url.openStream()?.use {
        responseString = String(it.readBytes())
    }
    return responseString
}